// LASER Pulse Controller
// Serial commands accepted
// PULSE X P-> Pulses UV LED for X miliseconds, power P
// TRAIN X Y Z P-> Makes a pulse train with X miliseconds on time, Y miliseconds off time, Z repeats, power P
// INTERRUPT -> Forces output to be 0 and stops execution

char inputArray[240];
String inputString = ""; 
boolean stringComplete = false;

int currentStatus = 0;
int oldStatus = 0;
unsigned long pulseOn;
unsigned long pulseOff;
int numPulses;
int currentPulse;
int oldPulse;
int pulsePower;

int LEDpin = 3;

int mainPWMCycle = 999;
volatile int cycleNumber = 0;
volatile int oldCycleNumber = 0;

unsigned long timer;

void setup() {

  inputString.reserve(240);
   
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);

  pinMode(LEDpin, OUTPUT);
  digitalWrite(LEDpin, LOW);
  
  Serial.begin(115200);
  
//  cli();
  DDRB=0x07;         // set OC1A pin output (among others)
  PORTB=0;  
  // Timer 1
  TCNT1=0;           // clear counter
  ICR1=mainPWMCycle;        
  
  TCCR1A=0b10000010; // non-inverting, fast PWM
  TCCR1B=0b00011001; // fast PWM, full speed
 
  OCR1A=0;         // 1 % strobe
  
  
  
  //TCCR0A = _BV(COM0A0) | _BV(COM0B1) | _BV(WGM01) | _BV(WGM00); 
  //TCCR0B = _BV(WGM02) | _BV(CS00);
 
  //OCR0A = mainPWMCycle;
  //OCR0B = 0;
}


void loop() {
  
  if (stringComplete){
    // Cleans input and separates in spaces
    inputString.trim();
    inputString.toCharArray(inputArray,240);
    char* command = strtok(inputArray, " ");
    
    // Checks commands and changes status
    if (strcmp(command,"PULSE")==0){
      
      command = strtok(0, " ");
      pulseOn = atof(command);
      command = strtok(0, " ");
      pulsePower = generateCycle(atof(command));
      currentStatus = 1;
      
    } else if (strcmp(command,"TRAIN")==0){
      
      command = strtok(0, " ");
      pulseOn = atof(command);
      command = strtok(0, " ");
      pulseOff = atof(command);
      command = strtok(0, " ");
      numPulses = atof(command);         
      command = strtok(0, " ");
      pulsePower = generateCycle(atof(command));
      currentStatus = 2;         
      
    } else if (strcmp(command,"INTERRUPT")==0){
      
      currentStatus = 0;
       
    }
  
    // Cleans variables used to receive serial data
    inputString = "";
    stringComplete = false;
    memset(&inputArray[0], 0, sizeof(inputArray));
  }
  
  // LED STATUS ZONE AND CONTROL
  switch (currentStatus){  
    case 0:
    {
      if (oldStatus != 0){
        OCR1A = 0;    
        Serial.println("[STOPPED]");
      }
      oldStatus = 0;
    }
      break;
      
    case 1:
    {
      if (oldStatus != 1){
        Serial.println("[EXECUTING] Single Pulse (" + String(pulseOn) + ")");
        timer = millis();
        OCR1A = pulsePower;

        pinMode(LEDpin, HIGH);
        
      }
      
      if (millis()-timer >= pulseOn){
        OCR1A = 0;
        
        pinMode(LEDpin, LOW);
        
        currentStatus = 0;
      }
      
      oldStatus = 1;   
    }  
      break; 
      
    case 2:
    {
      if (oldStatus != 2){
        currentPulse = 1;
        oldPulse = 0;
      }
      
      if (currentPulse > numPulses){
        currentStatus = 0;
        // Change pulser to 0
        OCR1A = 0;    
      } else {
        if (currentPulse > oldPulse){
          Serial.println("[EXECUTING: " + String(currentPulse) + "/" + String(numPulses) + "] Pulse Train (" + String(pulseOn) + " ON, " + String(pulseOff) + " OFF)");
          timer=millis();
          OCR1A = pulsePower;    
          oldPulse = currentPulse;
        }
        
        if (millis()-timer >= pulseOn){
          OCR1A = 0;    
          if (millis()-timer >= (pulseOn + pulseOff)){
            currentPulse++;
          }
        }
      }
      oldStatus = 2;
    }
      break;
  }
}


// Waits for serial data, and it's called everytime new data comes
// Full commands always end in '\n'
void serialEvent() 
{
  while (Serial.available()) 
  {
    char inChar = (char)Serial.read(); 
    inputString += inChar;
    if (inChar == '\n') 
    {
      stringComplete = true;
    } 
  }
}


int generateCycle(float input)
{
  return (int)(max(0,input*10-1));
}
