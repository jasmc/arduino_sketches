unsigned long timer;

int temp_int = 10;

float getTemp()
{

  // Converts input from a thermistor voltage divider to a temperature value.
  // The voltage divider consists of thermistor Rt and series resistor R0.
  // The value of R0 is equal to the thermistor resistance at T0.
  // You must set the following constants:
  //                  adcMax  ( ADC full range value )
  //                  analogPin (Arduino analog input pin)
  //                  invBeta  (inverse of the thermistor Beta value supplied by manufacturer).
  // Use Arduino's default reference voltage (5V or 3.3V) with this module.
  //

  const int analogPin = A0; // replace 0 with analog pin
  const float invBeta = 1.00 / 3976.00;   // replace "Beta" with beta of thermistor

  const  float adcMax = 1023.00;
  const float invT0 = 1.00 / 298.15;   // room temp in Kelvin

  int adcVal, i, numSamples = 5;
  float C;

  adcVal = 0;
  for (i = 0; i < numSamples; i++)
  {
    adcVal = adcVal + analogRead(analogPin);

  }
  adcVal = adcVal / numSamples;
  C = (1.00 / (invT0 - invBeta * (log ( adcMax / (float) adcVal - 1.00)))) - 273.15;

  return C;
}

void setup()
{
  analogReference(DEFAULT);
  Serial.begin(115200);
}

void loop()
{
  float temp = getTemp();
  timer = millis();

  if (timer%10 != 0)
  {
    Serial.print(timer); Serial.print(" ");
    Serial.println();
  }
  
  else if (timer%temp_int == 0)
  {   
    Serial.print(timer); Serial.print(" "); Serial.print(temp); Serial.print(" ");
    Serial.println();
    //delay(10);
    return;
  }
}
