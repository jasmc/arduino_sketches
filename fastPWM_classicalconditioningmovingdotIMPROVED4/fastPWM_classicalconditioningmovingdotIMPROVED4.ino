// Stimulus Controller
// Serial commands accepted
// STIM CSUS L/R X P N-> CS/US (0- only CS, 1- CS US paired, 2- only US), CS on the L or R side (0- left, 1- right, 2- left and right simultaneously, 3- no CS), US for X miliseconds with power P, number of repetitions N
// INTERRUPT -> Forces output to be 0 and stops execution


// Time each white CS LED is ON
const int Delay = 333;
// Number of the LED pin at which the US happens
const int trigLED = 8;

const int LEDnumberLeft[] = { 22, 23, 24, 25, 26, 27, 1, 2, 3, 4, 5, 6 };
const int LEDnumberRight[] = { 28, 29, 30, 31, 32, 53, 1, 2, 3, 4, 5, 6 };

int CSUS;
int LorR;
int numberReps;

String csus;
String lorr;

char inputArray[240];
String inputString = "";
boolean stringComplete = false;

int currentStatus = 0;
int oldStatus = 0;
unsigned long pulseOn;
unsigned long pulseOff;
int numPulses;
int currentPulse;
int oldPulse;
int pulsePower;

int j = 0;
int n = 12;
int currentRep = 0;
bool triggerUV = false;
bool newRep = true;
bool trialBeg = true;

int mainPWMCycle = 999;
volatile int cycleNumber = 0;
volatile int oldCycleNumber = 0;

unsigned long timer;
unsigned long timerUV;

void setup()
{

  inputString.reserve(240);

  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);

  for (int i = 0; i <= 11; i++) {
    pinMode(LEDnumberLeft[i], OUTPUT);
    pinMode(LEDnumberRight[i], OUTPUT);
    digitalWrite(LEDnumberLeft[i], LOW);
    digitalWrite(LEDnumberRight[i], LOW);
  }

  Serial.begin(115200);

  //  cli();
  DDRB = 0x07;       // set OC1A pin output (among others)
  PORTB = 0;
  // Timer 1
  TCNT1 = 0;         // clear counter
  ICR1 = mainPWMCycle;

  TCCR1A = 0b10000010; // non-inverting, fast PWM
  TCCR1B = 0b00011001; // fast PWM, full speed

  OCR1A = 0;       // 1 % strobe

  //TCCR0A = _BV(COM0A0) | _BV(COM0B1) | _BV(WGM01) | _BV(WGM00);
  //TCCR0B = _BV(WGM02) | _BV(CS00);

  //OCR0A = mainPWMCycle;
  //OCR0B = 0;
}


void loop()
{

  if (stringComplete)
  {
    // Cleans input and separates in spaces
    inputString.trim();
    inputString.toCharArray(inputArray, 240);
    char* command = strtok(inputArray, " ");

    // Checks commands and changes status
    if (strcmp(command, "STIM") == 0)
    {

      command = strtok(0, " ");
      CSUS = atof(command);

      command = strtok(0, " ");
      LorR = atof(command);

      command = strtok(0, " ");
      pulseOn = atof(command);
      command = strtok(0, " ");
      pulsePower = generateCycle(atof(command));

      command = strtok(0, " ");
      numberReps = atof(command);

      currentStatus = 1;
    }

    else if (strcmp(command, "INTERRUPT") == 0)
    {
      currentStatus = 0;
    }

    // Cleans variables used to receive serial data
    inputString = "";
    stringComplete = false;
    memset(&inputArray[0], 0, sizeof(inputArray));
  }

  // LED STATUS ZONE AND CONTROL
  switch (currentStatus)
  {
    case 0:
      {
        if (oldStatus != 0)
        {
          OCR1A = 0;
          for (int i = 0; i <= 11; i++) {
            digitalWrite(LEDnumberLeft[i], LOW);
            digitalWrite(LEDnumberRight[i], LOW);
          }
          Serial.println("[STOPPED]");
        }
        j = 0;
        currentRep = 0;
        oldStatus = 0;
      }
      break;

    case 1:
      {
        if (oldStatus != 1) {

          if (trialBeg) {

            if (CSUS == 0) {
              csus = "only CS";
            }
            else if (CSUS == 1) {
              csus = "CS->US";
            }
            else if (CSUS == 2) {
              csus = "only US";

              j = trigLED;
              Serial.println("Regardless of the input numberReps, a single pulse will be delivered.");

              //DON'T KNOW IF THIS IF IS NEEDED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              if (numberReps > 0) {
                numberReps = 1;
              }
            }

            else {
              Serial.println("Wrong input for CSUS!");
            }
            oldStatus = 1;


            if (LorR == 0) {
              lorr = "left side";
            }
            else if (LorR == 1) {
              lorr = "right side";
            }
            else if (LorR == 2) {
              lorr = "left and right sides";
            }
            else if (LorR == 3) {
              lorr = "no CS";
            }
            else {
              Serial.println("Wrong input for LorR!");
            }

            if (numberReps < 0 || pulseOn < 0 || pulsePower < 0) {
              Serial.println("Wrong input for numberReps, pulseOn or pulsePower!");
            }

            if (pulsePower > 100) {
              pulsePower = 100;
            }

            trialBeg = false;
          }

          if (newRep) {
            Serial.println("[EXECUTING] " + csus + " |  CS: " + lorr + " |  duration of US: " + pulseOn + " ms |  power: " + pulsePower + "% |  number of repetitions: " + numberReps + " |   current repetition: " + currentRep);

            timer = millis();

            switch (LorR) {
              case 0:
                {
                  digitalWrite(LEDnumberLeft[0], HIGH);
                  newRep = false;
                }
                break;
              case 1:
                {
                  digitalWrite(LEDnumberRight[0], HIGH);
                  newRep = false;
                }
                break;
              case 2:
                { digitalWrite(LEDnumberLeft[0], HIGH);
                  digitalWrite(LEDnumberRight[0], HIGH);
                  newRep = false;
                }
                break;
            }
          }

          //blink LEDs



          if (millis() - timer > Delay) {

            Serial.println("hei2");

            switch (LorR) {
              case 0: {
                  digitalWrite(LEDnumberLeft[j], LOW);
                  Serial.println("hei3");
                  j++;
                  if (j < n) {
                    digitalWrite(LEDnumberLeft[j], HIGH);
                    timer = millis();
                  }
                }
                break;

              case 1: {
                  digitalWrite(LEDnumberRight[j], LOW);
                  j++;
                  if (j < n) {
                    digitalWrite(LEDnumberRight[j], HIGH);
                    timer = millis();
                  }
                }

                break;

              case 2: {
                  digitalWrite(LEDnumberLeft[j], LOW);
                  j++;
                  if (j < n) {
                    digitalWrite(LEDnumberLeft[j], HIGH);
                    timer = millis();
                  }
                }
                break;
            }
          }

Serial.println("hei4");
          if (CSUS != 0 && j == trigLED && !triggerUV) {
            
            
            Serial.println("hei44");

            timerUV = millis();
            OCR1A = pulsePower;
            triggerUV = true;
          }

          else if (triggerUV && millis() - timerUV > pulseOn) {

             Serial.println("hei445");
            OCR1A = 0;
            triggerUV = false;

            if (CSUS == 2) {
              currentStatus = 0;
              //            oldStatus = 1;
              currentRep = 0;
              j = 0;
            }
          }

          if (j == n) {

            Serial.println("hei5");


            currentRep++;
            j = 0;
            newRep = true;
            if (currentRep == numberReps) {
              currentStatus = 0;
              oldStatus = 1;
              currentRep = 0;
            }
          }
          Serial.println("hei51");
        }

      }
      break;
  }

}



















// Waits for serial data, and it's called everytime new data comes
// Full commands always end in '\n'
void serialEvent()
{
  while (Serial.available())
  {
    char inChar = (char)Serial.read();
    inputString += inChar;
    if (inChar == '\n')
    {
      stringComplete = true;
    }
  }
}







int generateCycle(float input)
{
  return (int)(max(0, input * 10 - 1));
}
