const int clockPin = 6;
const int dataPin = 7;
const int latchPin = 5;

//const int resetPin = 8;

byte LEDs = 1;

void setup()
{

  //  pinMode(latchPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(clockPin, OUTPUT);

  //  pinMode(resetPin, OUTPUT);

  Serial.begin(115200);

}

void loop()
{

  for (int i = 0; i < 8; i++)
  {

    shiftOut(dataPin, clockPin, LSBFIRST, B00000001 << i);

    //TO COUPLE SHIFT REGISTERS IN SERIES
    shiftOut(dataPin, clockPin, LSBFIRST, B00000001 << 7 - i);
    //    digitalWrite(latchPin, HIGH);
    delay(200);
    //    digitalWrite(latchPin, LOW);


  }

}
