int clockPin = 3;
int dataPin = 4;

byte LEDs = 0;

void setup()
{
  pinMode(dataPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
}

void loop()
{
  LEDs = 0;
  updateShiftRegister();
  delay(1000);


  for (int i = 0; i < 8; i++)
  {

 //   digitalWrite(dataPin, HIGH);

    bitSet(LEDs, i);
    updateShiftRegister();
    delay(1000);

  }
}

void updateShiftRegister()
{
  //   digitalWrite(latchPin, LOW);
  digitalWrite(clockPin, LOW);
  shiftOut(dataPin, clockPin, LSBFIRST, LEDs);
  //   digitalWrite(latchPin, HIGH);
}
