
// Stimulus Controller
// Serial commands accepted
// STIM CSUS L/R X P Y N-> CS/US (0- only CS, 1- paired CS US, 2- only US), CS on the L or R side (0- left, 1- right, 2- left and right simultaneously, 3- no CS), US for X ms with power P after Y ms since the CS onset, number of repetitions N
// REINFORCER
// INTERRUPT -> Forces output to be 0 and stops execution

const int dataPin = 7;
const int clockPin = 5;
const int latchPin = 6;
const int outputEnablePin = 3;

String csus;
String lorr;
String inputString = "";
boolean stringComplete = false;
volatile int cycleNumber = 0;
volatile int oldCycleNumber = 0;

// Time each white CS LED is ON
const int Delay = 333;

// May change this in the future. It's to control the power of the CS LEDs with a (slow) PWM
const int powerCS = (255 - 100) / 255;

int CSUS;
int LorR;
int pulsePower;
int timeBefUS;
int numberReps;

int currentStatus = 0;
int oldStatus = 0;
int currentPulse;
int oldPulse;
int j = 1;
int n = 12;
int currentRep = 0;
int triggerUV = 0;
int triggerCS = 0;

const int mainPWMCycle = 999;

int reinforcer = 0;

bool newRep = true;
bool trialBeg = true;
char inputArray[240];

unsigned long pulseOn;
unsigned long timer;
unsigned long CSOnset;
unsigned long timerUV;

void setup()
{

  inputString.reserve(240);
  pinMode(dataPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(latchPin, OUTPUT);
  pinMode(outputEnablePin, OUTPUT);


  analogWrite(outputEnablePin, 0);

  cleanChip();

  delay(10);

  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);

  Serial.begin(115200);

  DDRB = 0x07; // set OC1A pin output (among others)
  PORTB = 0;

  TCNT1 = 0; // clear counter
  ICR1 = mainPWMCycle;

  TCCR1A = 0b10000010; // non-inverting, fast PWM
  TCCR1B = 0b00011001; // fast PWM, full speed

  OCR1A = mainPWMCycle; // 1 % strobe


  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, LSBFIRST, 0b00000001); // back right
  shiftOut(dataPin, clockPin, LSBFIRST, 0b00010000); // back left
  shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
  digitalWrite(latchPin, HIGH);






}

void loop()
{
  if (stringComplete)
  {
    // Cleans input and separates in spaces
    inputString.trim();
    inputString.toCharArray(inputArray, 240);
    char *command = strtok(inputArray, " ");
    // Checks commands and changes status
    if (currentStatus != 1 && strcmp(command, "STIM") == 0)
    {
      command = strtok(0, " ");
      CSUS = atof(command);
      command = strtok(0, " ");
      LorR = atof(command);
      command = strtok(0, " ");

      pulseOn = atof(command);
      command = strtok(0, " ");
      pulsePower = generateCycle(atof(command));
      command = strtok(0, " ");
      timeBefUS = atof(command);
      command = strtok(0, " ");
      numberReps = atof(command);
      currentStatus = 1;
    }

    else if (strcmp(command, "REINFORCER") == 0)
    {
      if (currentStatus == 0 && CSUS != 0)
      {
        Serial.println("Reinforcer not paired with CS or...");
        currentStatus = 0;
      }
      else
      {
        command = strtok(0, " ");
        pulseOn = atof(command);
        reinforcer = 1;
      }
    }

    else if (strcmp(command, "INTERRUPT") == 0)
    {
      oldStatus = 1; //To force entering in the INTERRUPT loop.                      CHECK
      currentStatus = 0;
    }
    // Cleans variables used to receive serial data
    inputString = "";
    stringComplete = false;
    memset(&inputArray[0], 0, sizeof(inputArray));
  }

  // LED STATUS ZONE AND CONTROL
  switch (currentStatus)
  {
    case 0:
      if (oldStatus != 0)
      {
        OCR1A = mainPWMCycle;
        cleanChip();
        j = 1;
        trialBeg = true;
        newRep = true;
        currentRep = 0;
        triggerUV = 0;
        triggerCS = 0;
        oldStatus = 0;
        reinforcer = 0;
        Serial.println("[STOPPED]");
      }
      break;
    case 1:
      if (oldStatus != 1)
      {
        if (trialBeg)
        {
          if (CSUS == 0)
          {
            csus = "only CS";
          }
          else if (CSUS == 1)
          {
            csus = "CS->US";
          }
          else if (CSUS == 2)
          {
            csus = "only US";
            LorR = 3;
            Serial.println("Regardless of the input numberReps, a single pulse will be delivered.");
            numberReps = 1;
          }
          else
          {
            Serial.println("Wrong input for CSUS!");
          }

          if (LorR == 0)
          {
            lorr = "left side";
          }
          else if (LorR == 1)
          {
            lorr = "right side";
          }
          else if (LorR == 2)
          {
            lorr = "left and right sides";
          }
          else if (CSUS == 2 && LorR == 3)
          {
            lorr = "only US";
          }
          else
          {
            currentStatus = 0;
            oldStatus = 1;
            Serial.println("Wrong input for LorR!");
          }

          if (numberReps <= 0 || pulseOn < 0 || timeBefUS < 0 || pulsePower < 0)
          {
            Serial.println("Wrong input for numberReps, pulseOn, pulsePower or timeBefUS!");
          }
          trialBeg = false;
        }

        if (newRep)
        {
          Serial.println("[EXECUTING] " + csus + " |  CS: " + lorr + " |  duration of US: " + pulseOn + " ms |  latency of US: " + timeBefUS + " ms |  number of repetitions: " + numberReps + " |   current repetition: " + currentRep);

          newRep = false;

          if (CSUS != 2)
          {
            timer = millis();
            CSOnset = millis();
            triggerCS = 1;
            digitalWrite(latchPin, LOW);
            switch (LorR)
            {
              case 0:
                shiftOut(dataPin, clockPin, LSBFIRST, 0b00000001); //back right
                shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
                shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000); //front left
                break;
              case 1:
                shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
                shiftOut(dataPin, clockPin, LSBFIRST, 0b00011000); //front right and back left
                shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
                break;
              case 2:
                shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
                shiftOut(dataPin, clockPin, LSBFIRST, 0b00001000);
                shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000);
                break;
            }
            digitalWrite(latchPin, HIGH);
          }
        }

        if (CSUS != 2)
        {
          if (millis() - timer >= Delay)
          {
            timer = millis();
            if (j < n)
            {
              digitalWrite(latchPin, LOW);
              switch (LorR)
              {
                case 0:
                  if (j < 8)
                  {
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b00000001 << j);
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000 >> j);
                  }
                  else if (j < 12)
                  {
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
                    shiftOut(dataPin, clockPin, LSBFIRST, (0b10000000 >> j - 8) ^ (0b00000001 << j - 8));
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
                  }
                  break;
                case 1:
                  if (j < 4)
                  {
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
                    shiftOut(dataPin, clockPin, LSBFIRST, (0b00001000 >> j) ^ (0b00010000 << j));
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
                  }
                  else if (j < 12)
                  {
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000 >> j - 4);
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b00000001 << j - 4);
                  }
                  break;
                case 2:
                  if (j < 4)
                  {
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b00001000 >> j);
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000 >> j);
                  }
                  else if (j >= 4 && j < 8)
                  {
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000 >> j - 4);
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b00001000 >> j - 4);
                  }
                  else if (j < 12)
                  {
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b00001000 >> j - 8);
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000 >> j - 8);
                    shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
                  }
                  break;
              }
              digitalWrite(latchPin, HIGH);
              j++;
            }
            else
            {
              cleanChip();
              triggerCS = 0;
            }
          }
        }
// Switch on the UV LED
        if (triggerUV == 0 && ((CSUS == 0 && reinforcer == 1) || (CSUS == 1 && millis() - CSOnset >= timeBefUS) || CSUS == 2))
        {
          timerUV = millis();
          OCR1A = pulsePower;
          triggerUV = 1; //triggerUV = 1 means that the UV LED is blinking
        }
// Switch off the UV LED
        else if (triggerUV == 1 && millis() - timerUV >= pulseOn)
        {
          OCR1A = mainPWMCycle;
          triggerUV = 2; //triggerUV = 2 means that the UV LED has already blinked
        }

        else if ((CSUS == 0 && j == n && triggerUV != 1 && triggerCS == 0) || (CSUS == 1 && j == n && triggerUV == 2 && triggerCS == 0) || (CSUS == 2 && triggerUV == 2))
        {
          currentRep++;
          j = 1;
          newRep = true;
          triggerUV = 0;
          if (currentRep >= numberReps)
          {
            currentStatus = 0;
            oldStatus = 1;
            currentRep = 0;
          }
        }
      }
      break;
  }
}

// Waits for serial data, and it's called everytime new data comes
// Full commands always end in '\n'
void serialEvent()
{
  while (Serial.available())
  {
    char inChar = (char)Serial.read();
    inputString += inChar;
    if (inChar == '\n')
    {
      stringComplete = true;
    }
  }
}

int generateCycle(float input)
{
  return (int)(min(mainPWMCycle, mainPWMCycle - input * 10 + 1));
}

int powerLEDCS(float powerCS)
{
  return (int)((255 - powerCS) / 255);
}

void cleanChip()
{
  analogWrite(outputEnablePin, 0);
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, LSBFIRST, 0b00000001); // back right
  shiftOut(dataPin, clockPin, LSBFIRST, 0b00010000); // back left
  shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
  digitalWrite(latchPin, HIGH);
  analogWrite(outputEnablePin, 0);
}
