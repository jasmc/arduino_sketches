//Adapted to blink an LED  whenever the laser goes ON
// LED and Laser Pulse Controller


// Serial commands accepted
// PULSE X -> Pulses UV LED for X miliseconds
// TRAIN X Y Z -> Makes a pulse train with X miliseconds on time, Y miliseconds off time, Z repeats
// INTERRUPT -> Forces output to be 0 and stops execution

char inputArray[240];
String inputString = ""; 
boolean stringComplete = false;

int currentStatus = 0;
int oldStatus = 0;
unsigned long pulseOn;
unsigned long pulseOff;
int numPulses;
int currentPulse;
int oldPulse;
int sigPin = 9;
int soundPin = 4;

int sigPinLED = 11;
int timeLED = 100;
int LED = 0;


unsigned long timer;

void setup() {

  inputString.reserve(240);
   
  pinMode(sigPin, OUTPUT);

  pinMode(sigPinLED, OUTPUT);

 
  pinMode(soundPin, OUTPUT);
  
  digitalWrite(soundPin,LOW);
  
  
  Serial.begin(115200); 
}


void loop() {
  
  if (stringComplete){
    // Cleans input and separates in spaces
    inputString.trim();
    inputString.toCharArray(inputArray,240);
    char* command = strtok(inputArray, " ");
    
    // Checks commands and changes status
    if (strcmp(command,"PULSE")==0){

      timer = millis();
      Serial.print(timer); Serial.print(" ");
      Serial.println();
      
      
      command = strtok(0, " ");
      pulseOn = atof(command);
      currentStatus = 1;
      
    } else if (strcmp(command,"TRAIN")==0){
      
      command = strtok(0, " ");
      pulseOn = atof(command);
      command = strtok(0, " ");
      pulseOff = atof(command);
      command = strtok(0, " ");
      numPulses = atof(command);         
      currentStatus = 2;         
      
    } else if (strcmp(command,"INTERRUPT")==0){
      
      currentStatus = 0;
       
    }
  
    // Cleans variables used to receive serial data
    inputString = "";
    stringComplete = false;
    memset(&inputArray[0], 0, sizeof(inputArray));
  }
  
  // LED STATUS ZONE AND CONTROL
  switch (currentStatus){  
    case 0:
    {
      if (oldStatus != 0){
        digitalWrite(sigPin, LOW);

        digitalWrite(sigPinLED, LOW);
        
        digitalWrite(soundPin, LOW);
        Serial.println("[STOPPED]");
      }
      oldStatus = 0;
    }
      break;
      
    case 1:
    {
      if (oldStatus != 1){
        Serial.println("[EXECUTING] Single Pulse (" + String(pulseOn) + ")");
        timer = millis();

        digitalWrite(sigPinLED, HIGH);
        LED = 0;
        
        digitalWrite(sigPin, HIGH);
        digitalWrite(soundPin, HIGH);
      }
      

      if (LED == 0 && millis()-timer >= timeLED){
        digitalWrite(sigPinLED, LOW);
        LED = 1;
      }

      
      if (millis()-timer >= pulseOn){     
        digitalWrite(sigPin, LOW);
        digitalWrite(soundPin, LOW);
        currentStatus = 0;
      }
      
      oldStatus = 1;   
    }  
      break; 
      
    case 2:
    {
      if (oldStatus != 2){
        currentPulse = 1;
        oldPulse = 0;
      }
      
      if (currentPulse > numPulses){
        currentStatus = 0;
        digitalWrite(sigPin, LOW);
        digitalWrite(soundPin, LOW);
      } else {
        if (currentPulse > oldPulse){
          Serial.println("[EXECUTING: " + String(currentPulse) + "/" + String(numPulses) + "] Pulse Train (" + String(pulseOn) + " ON, " + String(pulseOff) + " OFF)");
          timer=millis();
          digitalWrite(sigPin, HIGH);
          digitalWrite(soundPin, HIGH);
          oldPulse = currentPulse;
        }
        
        if (millis()-timer >= pulseOn){
          digitalWrite(sigPin, LOW);
           digitalWrite(soundPin, LOW);
          if (millis()-timer >= (pulseOn + pulseOff)){
            currentPulse++;
          }
        }
      }
      oldStatus = 2;
    }
      break;
  }
}


// Waits for serial data, and it's called everytime new data comes
// Full commands always end in '\n'
void serialEvent() 
{
  while (Serial.available()) 
  {
    char inChar = (char)Serial.read(); 
    inputString += inChar;
    if (inChar == '\n') 
    {
      stringComplete = true;
    } 
  }
}
