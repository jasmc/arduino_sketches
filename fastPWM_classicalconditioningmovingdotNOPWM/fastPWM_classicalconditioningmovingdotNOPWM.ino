// Stimulus Controller
// Serial commands accepted
// STIM CSUS L/R X P N-> CS/US (0- only CS, 1- paired CS US, 2- only US), CS on the L or R side (0- left, 1- right, 2- left and right simultaneously, 3- no CS), US for X miliseconds with power P, number of repetitions N
// INTERRUPT -> Forces output to be 0 and stops execution

// Number of the LED pin at which the US happens
const int trigLED = 8;
const int LEDnumberLeft[] = { 22, 23, 24, 25, 26, 27, 7, 8, 9, 10, 12, 13 };
const int LEDnumberRight[] = { 28, 29, 30, 31, 32, 34, 7, 8, 9, 10, 12, 13 };
String csus;
String lorr;
String inputString = "";
boolean stringComplete = false;
volatile int cycleNumber = 0;
volatile int oldCycleNumber = 0;

// Time each white CS LED is ON
int Delay = 100;
int DelayOld = Delay;
int CSUS;
int LorR;
int numberReps;
int currentStatus = 0;
int oldStatus = 0;
int currentPulse;
int oldPulse;
int pulsePower;
int j = 0;
int n = 12;
int currentRep = 0;
int triggerUV = 0;
int mainPWMCycle = 999;
bool newRep = true;
bool trialBeg = true;
char inputArray[240];
unsigned long pulseOn;
unsigned long timer;
unsigned long timerUV;

void setup() {
  inputString.reserve(240);
  pinMode(11, OUTPUT);
  digitalWrite(11, LOW);
  for (int i = 0; i <= 11; i++) {
    pinMode(LEDnumberLeft[i], OUTPUT);
    pinMode(LEDnumberRight[i], OUTPUT);
    digitalWrite(LEDnumberLeft[i], LOW);
    digitalWrite(LEDnumberRight[i], LOW);
  }
  Serial.begin(115200);
}

void loop()
{
  if (stringComplete)
  {
    // Cleans input and separates in spaces
    inputString.trim();
    inputString.toCharArray(inputArray, 240);
    char* command = strtok(inputArray, " ");
    // Checks commands and changes status
    if (strcmp(command, "STIM") == 0) {
      command = strtok(0, " ");
      CSUS = atof(command);
      command = strtok(0, " ");
      LorR = atof(command);
      command = strtok(0, " ");
      pulseOn = atof(command);
      command = strtok(0, " ");
      pulsePower = generateCycle(atof(command));
      command = strtok(0, " ");
      numberReps = atof(command);
      currentStatus = 1;
    }
    else if (strcmp(command, "INTERRUPT") == 0) {
      currentStatus = 0;
    }
    // Cleans variables used to receive serial data
    inputString = "";
    stringComplete = false;
    memset(&inputArray[0], 0, sizeof(inputArray));
  }
  // LED STATUS ZONE AND CONTROL
  switch (currentStatus)
  {
    case 0:
      {
        if (oldStatus != 0) {
          digitalWrite(11, LOW);
          for (int i = 0; i <= 11; i++) {
            digitalWrite(LEDnumberLeft[i], LOW);
            digitalWrite(LEDnumberRight[i], LOW);
          }
          Serial.println("[STOPPED]");
        }
        j = 0;
        trialBeg = true;
        currentRep = 0;
        oldStatus = 0;
      }
      break;
    case 1:
      {
        if (oldStatus != 1) {

          if (trialBeg) {

            if (CSUS == 0) {
              csus = "only CS";
            }
            else if (CSUS == 1) {
              csus = "CS->US";
            }
            else if (CSUS == 2) {
              csus = "only US";
              Delay = 0;
              j = trigLED;
              LorR = 3;
              Serial.println("Regardless of the input numberReps, a single pulse will be delivered.");
              numberReps = 1;
            }
            else {
              Serial.println("Wrong input for CSUS!");
            }

            if (LorR == 0) {
              lorr = "left side";
            }
            else if (LorR == 1) {
              lorr = "right side";
            }
            else if (LorR == 2) {
              lorr = "left and right sides";
            }
            else if (LorR == 3) {
              lorr = "no CS";
            }
            else {
              Serial.println("Wrong input for LorR!");
            }

            if (numberReps < 0 || pulseOn < 0 || pulsePower < 0) {
              Serial.println("Wrong input for numberReps, pulseOn or pulsePower!");
            }

            if (pulsePower > 100) {
              pulsePower = 100;
            }

            trialBeg = false;
          }

          if (newRep) {
            Serial.println("[EXECUTING] " + csus + " |  CS: " + lorr + " |  duration of US: " + pulseOn + " ms |  power: " + pulsePower + "% |  number of repetitions: " + numberReps + " |   current repetition: " + currentRep);

            timer = millis();
            Serial.println(timer);

            switch (LorR) {
              case 0:
                {
                  digitalWrite(LEDnumberLeft[0], HIGH);
                }
                break;
              case 1:
                {
                  digitalWrite(LEDnumberRight[0], HIGH);
                }
                break;
              case 2:
                { digitalWrite(LEDnumberLeft[0], HIGH);
                  digitalWrite(LEDnumberRight[0], HIGH);
                }
                break;
              case 3:
                break;
            }
            newRep = false;
          }

          if (millis() - timer > Delay) {

            switch (LorR) {
              case 0: {
                  digitalWrite(LEDnumberLeft[j], LOW);
                  if (j < n) {
                    j++;
                    timer = millis();
                    if (j != n) {
                      digitalWrite(LEDnumberLeft[j], HIGH);
                    }
                  }
                }
                break;

              case 1: {
                  digitalWrite(LEDnumberRight[j], LOW);
                  if (j < n) {
                    j++;
                    timer = millis();
                    if (j != n) {
                      digitalWrite(LEDnumberRight[j], HIGH);
                    }
                  }
                }
                break;

              case 2: {
                  digitalWrite(LEDnumberLeft[j], LOW);
                  digitalWrite(LEDnumberRight[j], LOW);
                  if (j < n) {
                    j++;
                    timer = millis();
                    if (j != n) {
                      digitalWrite(LEDnumberLeft[j], HIGH);
                      digitalWrite(LEDnumberRight[j], HIGH);
                    }
                  }
                }
                break;
              case 3: {
                  //This is in case I want to replicate the CS-US but without CS...
                  //                  j++;
                  //                  if (j < n) {
                  //                    timer = millis();
                  //                    }
                }
                break;
            }
          }

          if (triggerUV == 0 && CSUS != 0 && j == trigLED) {
            timerUV = millis();
            //            OCR1A = pulsePower;
            digitalWrite(11, HIGH);
            triggerUV = 1;
          }

          else if (triggerUV == 1 && millis() - timerUV > pulseOn) {
            //            OCR1A = 0;
            digitalWrite(11, LOW);
            triggerUV = 2;
            if (CSUS == 2) {
              currentStatus = 0;
              oldStatus = 1;
              j = trigLED;
              newRep = true;
              bool trialBeg = true;
              triggerUV = 0;
              DelayOld = Delay;
            }
          }

          if ((j == n && triggerUV == 2 && CSUS == 1) || (j == n && CSUS == 0)) {
            currentRep++;
            j = 0;
            newRep = true;
            triggerUV = 0;
            if (currentRep == numberReps) {
              currentStatus = 0;
              oldStatus = 1;
              currentRep = 0;
            }
          }
        }
      }
      break;
  }
}

// Waits for serial data, and it's called everytime new data comes
// Full commands always end in '\n'
void serialEvent()
{
  while (Serial.available())
  {
    char inChar = (char)Serial.read();
    inputString += inChar;
    if (inChar == '\n')
    {
      stringComplete = true;
    }
  }
}


int generateCycle(float input)
{
  return (int)(max(0, input * 10 - 1));
}
