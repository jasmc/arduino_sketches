// LASER Pulse Controller
// Serial commands accepted
// PULSE X P-> Pulses UV LED for X miliseconds, power P
// INTERRUPT -> Forces output to be 0 and stops execution

// Time each white LED is ON
const int Delay = 333;
// Number of the LED at which the stimulus happens
const int trigLED = 30;

char inputArray[240];
String inputString = "";
boolean stringComplete = false;

int currentStatus = 0;
int oldStatus = 0;
unsigned long pulseOn;
unsigned long pulseOff;
int numPulses;
int currentPulse;
int oldPulse;
int pulsePower;

const int LEDnumber[] = { 22,23,24,25,26,27,28,29,30,31,32,53 };

// { 1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13 };

int j = 0;
int n = 12;
bool trialEnd = false;
bool triggerUV = false;


int mainPWMCycle = 999;
volatile int cycleNumber = 0;
volatile int oldCycleNumber = 0;

unsigned long timer;
unsigned long timerUV;

void setup()
{

  inputString.reserve(240);

  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);

  for (int i = 0; i <= 10; i++) {
    pinMode(LEDnumber[i], OUTPUT);
    digitalWrite(LEDnumber[i], LOW);
  }

  Serial.begin(115200);

  //  cli();
  DDRB = 0x07;       // set OC1A pin output (among others)
  PORTB = 0;
  // Timer 1
  TCNT1 = 0;         // clear counter
  ICR1 = mainPWMCycle;

  TCCR1A = 0b10000010; // non-inverting, fast PWM
  TCCR1B = 0b00011001; // fast PWM, full speed

  OCR1A = 0;       // 1 % strobe

  //TCCR0A = _BV(COM0A0) | _BV(COM0B1) | _BV(WGM01) | _BV(WGM00);
  //TCCR0B = _BV(WGM02) | _BV(CS00);

  //OCR0A = mainPWMCycle;
  //OCR0B = 0;
}


void loop()
{

  if (stringComplete)
  {
    // Cleans input and separates in spaces
    inputString.trim();
    inputString.toCharArray(inputArray, 240);
    char* command = strtok(inputArray, " ");

    // Checks commands and changes status
    if (strcmp(command, "PULSE") == 0)
    {
      command = strtok(0, " ");
      pulseOn = atof(command);
      command = strtok(0, " ");
      pulsePower = generateCycle(atof(command));
      currentStatus = 1;
    }

    else if (strcmp(command, "INTERRUPT") == 0)
    {
      currentStatus = 0;
    }





    else if (strcmp(command, "TEST") == 0)
    {
      command = strtok(0, " ");
      pulseOn = atof(command);
      command = strtok(0, " ");
      pulsePower = generateCycle(atof(command));
      currentStatus = 2;
    }





    // Cleans variables used to receive serial data
    inputString = "";
    stringComplete = false;
    memset(&inputArray[0], 0, sizeof(inputArray));
  }

  // LED STATUS ZONE AND CONTROL
  switch (currentStatus)
  {
    case 0:
      {
        if (oldStatus != 0)
        {
          OCR1A = 0;

          for (int i = 0; i <= 10; i++) {
            digitalWrite(LEDnumber[i], LOW);
          }

          Serial.println("[STOPPED]");
        }
        j = 0;
        trialEnd = false;
        oldStatus = 0;
      }
      break;

    case 1:
      {
        if (oldStatus != 1)
        {
          j = 0;
          Serial.println("[EXECUTING] Single Pulse (" + String(pulseOn) + ")");
          digitalWrite(LEDnumber[0], HIGH);
          timer = millis();
        }

        //blink LEDs

        if (millis() - timer > Delay && !trialEnd)
        {
          digitalWrite(LEDnumber[j], LOW);
          j++;
          timer = millis();

          if (j < n)
          {
            digitalWrite(LEDnumber[j], HIGH);

            if (j == trigLED && !triggerUV)
            {
              triggerUV = true;
              timerUV = millis();
              OCR1A = pulsePower;
            }
          }
          else
          {
            trialEnd = true;
            currentStatus = 0;
          }
        }

        if (triggerUV && millis() - timerUV >= pulseOn)
        {
          OCR1A = 0;
        }
        oldStatus = 1;
      }

      break;

    case 2:
      {
        if (oldStatus != 2)
        {
          j = 0;
          Serial.println("[EXECUTING] TEST PULSE");
          digitalWrite(LEDnumber[0], HIGH);
          timer = millis();
        }

        //blink LEDs

        if (millis() - timer > Delay && !trialEnd)
        {
          digitalWrite(LEDnumber[j], LOW);
          j++;
          timer = millis();

          if (j < n)
          {
            digitalWrite(LEDnumber[j], HIGH);
          }
          else
          {
            trialEnd = true;
            currentStatus = 0;
          }
        }
        oldStatus = 2;
      }
      break;
  }
}



// Waits for serial data, and it's called everytime new data comes
// Full commands always end in '\n'
void serialEvent()
{
  while (Serial.available())
  {
    char inChar = (char)Serial.read();
    inputString += inChar;
    if (inChar == '\n')
    {
      stringComplete = true;
    }
  }
}







int generateCycle(float input)
{
  return (int)(max(0, input * 10 - 1));
}
