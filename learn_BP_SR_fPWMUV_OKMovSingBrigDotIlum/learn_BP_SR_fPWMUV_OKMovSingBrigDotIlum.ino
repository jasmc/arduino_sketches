// Stimulus Controller
// Serial commands accepted
//* STIM CSUS L/R X P Y N
// -> CS/US (0- only CS, 1- paired CS US, 2- only US), CS on the L or R side
//(0- left, 1- right, 2- left and right simultaneously, 3- no CS), US for X ms with power P
// after Y ms since the CS onset, number of repetitions N
//* OFF -> Immediately turn off all lights.

// May change this in the future. It's to control the power of the CS LEDs with a (slow) PWM
// const int powerCS = (255 - 100) / 255;
char inputArray[240];
String inputString = "";
boolean stringComplete = false;
boolean newRep = true;
boolean trialBeg = true;
boolean isClean = false;
boolean realClean = false;
// Time each white CS LED is ON
const int Delay = 333;
const int mainPWMCycle = 999;
const int dataPin = 7;
const int clockPin = 5;
const int latchPin = 6;
const int outputEnablePin = 3;
const int DAQPin = 4;
unsigned int CSUS;
unsigned int LorR;
unsigned int currentStatus = 0;
unsigned int oldStatus = 0;
unsigned int triggerUV = 0;
unsigned int pulsePower;
unsigned int pulsepower;
unsigned int pulseLatency;
unsigned int numberReps;
unsigned int j = 1;
unsigned int n = 12;
unsigned int currentRep = 0;
unsigned int reinforcer = 0;
unsigned long pulseDuration;
unsigned long timer;
unsigned long CSOnset;
unsigned long timerUV;

//#define DEBUG 0

void setup()
{
	pinMode(DAQPin, OUTPUT);
	digitalWrite(DAQPin, LOW);
	inputString.reserve(240);
	pinMode(dataPin, OUTPUT);
	pinMode(clockPin, OUTPUT);
	pinMode(latchPin, OUTPUT);
	pinMode(outputEnablePin, OUTPUT);
	cleanChip(realClean);
	delay(10);
	pinMode(9, OUTPUT);
	pinMode(10, OUTPUT);
	Serial.begin(115200);
	DDRB = 0x07; // set OC1A pin output (among others)
	PORTB = 0;
	TCNT1 = 0; // clear counter
	ICR1 = mainPWMCycle;
	TCCR1A = 0b10000010;  // non-inverting, fast PWM
	TCCR1B = 0b00011001;  // fast PWM, full speed
	OCR1A = mainPWMCycle; // 1 % strobe
	cleanChip(realClean);
}

void loop()
{
	if (stringComplete)
	{
		// Cleans input and separates in spaces
		inputString.trim();
		inputString.toCharArray(inputArray, 240);
		char *command = strtok(inputArray, " ");

		// Checks commands and changes status
		if (strcmp(command, "STIM") == 0)
		{
			if (currentStatus != 1)
			{
				command = strtok(0, " ");
				CSUS = atof(command);
				command = strtok(0, " ");
				LorR = atof(command);
				command = strtok(0, " ");
				pulseDuration = atof(command);
				command = strtok(0, " ");
				pulsepower = atof(command);
				command = strtok(0, " ");
				pulseLatency = atof(command);
				command = strtok(0, " ");
				numberReps = atof(command);

				pulsePower = generateCycle(pulsepower);

				currentStatus = 1;
			}
			else
			{
				command = strtok(0, " ");

				reinforcer = atof(command);

				if (reinforcer == 2)
				{
					command = strtok(0, " ");
					command = strtok(0, " ");
					pulseDuration = atof(command);
					command = strtok(0, " ");
					pulsepower = atof(command);

					pulsePower = generateCycle(pulsepower);

					triggerUV = 1;
				}
			}
		}

		else if (strcmp(command, "BREAK") == 0)
		{
			currentStatus = 0;
			oldStatus = 1;
			realClean = false;
		}

		else if (strcmp(command, "OFF") == 0)
		{
			currentStatus = 0;
			oldStatus = 1;
			realClean = true;
		}

		else if (strcmp(command, "STARTACQUISITION") == 0)
		{
			digitalWrite(DAQPin, HIGH);
		}

		else if (strcmp(command, "STOPACQUISITION") == 0)
		{
			digitalWrite(DAQPin, LOW);
		}

		// Cleans variables used to receive serial data
		inputString = "";
		stringComplete = false;
		memset(&inputArray[0], 0, sizeof(inputArray));
	}

	// LED STATUS ZONE AND CONTROL
	switch (currentStatus)
	{
	case 0:
		if (oldStatus != 0)
		{
			isClean = false;
			if (triggerUV == 2)
			{
				OCR1A = mainPWMCycle;
				triggerUV = 3;
			}

			cleanChip(realClean);

			j = 1;
			trialBeg = true;
			newRep = true;
			currentRep = 0;
			oldStatus = 0;
			triggerUV = 0;
			CSUS = 0;
		}
		break;

	case 1:
		realClean = false;
		if (oldStatus != 1)
		{
			if (trialBeg)
			{
				trialBeg = false;
				if (CSUS != 0)
				{
					triggerUV = 1;
					numberReps = 1;
					if (CSUS == 2)
					{
						LorR = 3;
					}
				}
			}

			if (newRep)
			{
				newRep = false;
				isClean = false;

				if (CSUS != 2)
				{
					timer = millis();
					CSOnset = millis();

					digitalWrite(latchPin, LOW);

					if (currentRep == 0)
					{
						Serial.println("B");
					}

					switch (LorR)
					{
					case 0:
						shiftOut(dataPin, clockPin, LSBFIRST, 0b00000001); // back right
						shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
						shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000); // front left
						break;
					case 1:
						shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
						shiftOut(dataPin, clockPin, LSBFIRST, 0b00011000); // front right and back left
						shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
						break;
					case 2:
						shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
						shiftOut(dataPin, clockPin, LSBFIRST, 0b00001000);
						shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000);
						break;
					}
					digitalWrite(latchPin, HIGH);
				}
			}

			if (CSUS != 2)
			{
				if (millis() - timer >= Delay)
				{
					timer = millis();

					if (j < n)
					{
						digitalWrite(latchPin, LOW);
						switch (LorR)
						{
						case 0:
							if (j < 8)
							{
								shiftOut(dataPin, clockPin, LSBFIRST, 0b00000001 << j);
								shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
								shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000 >> j);
							}
							else if (j < 12)
							{
								shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
								shiftOut(dataPin, clockPin, LSBFIRST, (0b10000000 >> j - 8) ^ (0b00000001 << j - 8));
								shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
							}
							break;
						case 1:
							if (j < 4)
							{
								shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
								shiftOut(dataPin, clockPin, LSBFIRST, (0b00001000 >> j) ^ (0b00010000 << j));
								shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
							}
							else if (j < 12)
							{
								shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000 >> j - 4);
								shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
								shiftOut(dataPin, clockPin, LSBFIRST, 0b00000001 << j - 4);
							}
							break;
						case 2:
							if (j < 4)
							{
								shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
								shiftOut(dataPin, clockPin, LSBFIRST, 0b00001000 >> j);
								shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000 >> j);
							}
							else if (j >= 4 && j < 8)
							{
								shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000 >> j - 4);
								shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
								shiftOut(dataPin, clockPin, LSBFIRST, 0b00001000 >> j - 4);
							}
							else if (j < 12)
							{
								shiftOut(dataPin, clockPin, LSBFIRST, 0b00001000 >> j - 8);
								shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000 >> j - 8);
								shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
							}
							break;
						}
						digitalWrite(latchPin, HIGH);
						j++;
					}

					else
					{
						if (!isClean)
						{
							cleanChip(realClean);
							Serial.println("E");
							isClean = true;
						}

						if (CSUS == 0 || (CSUS == 1 && triggerUV == 3))
						{
							currentRep++;

							if (currentRep == numberReps)
							{
								currentStatus = 0;
								oldStatus = 1;
							}
							newRep = true;
							j = 1;
						}
					}
				}
			}

			// Switch on the UV LED
			if (triggerUV == 1 && (CSUS == 0 || (CSUS == 1 && millis() - CSOnset >= pulseLatency) || CSUS == 2))
			{
				timerUV = millis();
				OCR1A = pulsePower;
				Serial.println("b");
				triggerUV = 2; // triggerUV == 2 means that the UV LED is blinking.
			}

			// Switch off the UV LED
			else if (triggerUV == 2 && millis() - timerUV >= pulseDuration)
			{
				OCR1A = mainPWMCycle;
				Serial.println("e");

				triggerUV = 3; // triggerUV == 3 means that the UV LED has already blinked

				if (CSUS == 2)
				{
					currentStatus = 0;
					oldStatus = 1;
				}
			}
			break;
		}
	}
}

// Waits for serial data, and it's called everytime new data comes
// Full commands always end in '\n'
void serialEvent()
{
	while (Serial.available())
	{
		if (!stringComplete)
		{
			char inChar = (char)Serial.read();
			inputString += inChar;
			if (inChar == '\n')
				stringComplete = true;
		}
	}
}

int generateCycle(float input)
{
	return (int)(min(mainPWMCycle, mainPWMCycle - input * 10 + 1));
}

void cleanChip(boolean realClean)
{
	analogWrite(outputEnablePin, 0); // OE (Output Enable) pin is always at 0 V, meaning that the ouput pins are always enabled.
	digitalWrite(latchPin, LOW);
	if (realClean)
	{
		//! Here could use the SRCLR (Shift Register Clear) pin, but this pin is connected to VCC.
		shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
		shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
		shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
	}
	else
	{
		// analogWrite(outputEnablePin, 0);
		// digitalWrite(latchPin, LOW);
		shiftOut(dataPin, clockPin, LSBFIRST, 0b00000000);
		shiftOut(dataPin, clockPin, LSBFIRST, 0b00001000); // front right
		shiftOut(dataPin, clockPin, LSBFIRST, 0b10000000); // front left
		// digitalWrite(latchPin, HIGH);
		// analogWrite(outputEnablePin, 0);
	}
	digitalWrite(latchPin, HIGH);
	analogWrite(outputEnablePin, 0);
}

// int powerLEDCS(float powerCS)
//{
//   return (int)((255 - powerCS) / 255);
// }